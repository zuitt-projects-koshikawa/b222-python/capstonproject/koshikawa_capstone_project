from abc import ABC, abstractclassmethod

class Person(ABC):
    @abstractclassmethod
    def get_full_name(self):
        pass
    
    @abstractclassmethod
    def add_request(self):
        pass
    
    @abstractclassmethod
    def check_request(self):
        pass
    
    @abstractclassmethod
    def add_user(self):
        pass


class Employee(Person):
    def __init__(self, first_name, last_name, email, department):
        self._first_name = first_name
        self._last_name = last_name
        self._email = email
        self._department = department
        
    # Getter 
    def get_first_name(self):
        return f"My first name is {self._first_name}." 
    
    def get_last_name(self):
        return f"My last name is {self._last_name}." 
        
    def get_email(self):
        return f"My email is {self._email}." 
    
    def get_department(self):
        return f"My department is {self._department}." 
       
    # Setter 
    def set_first_name(self, first_name):
        self._first_name = first_name
    
    def set_last_name(self, last_name):
        self._last_name = last_name
    
    def set_email(self, email):
        self._email = email
    
    def set_department(self, department):
        self._department = department
    
    # Abstract methods
    def get_full_name(self):
        full_name = f"{self._first_name} {self._last_name}"
        return full_name
        
    def add_request(self):
        return "Request has been added"
    
    def check_request(self):
        pass
    
    def add_user(self):
        pass
    
    def login(self):
        message = f"{self._email} has logged in"
        return message

    
    def logout(self):
        return f"{self._email} has logged out"

class TeamLead(Person):
    def __init__(self,first_name, last_name, email, department):
        self._first_name = first_name
        self._last_name = last_name
        self._email = email
        self._department = department
        self._members = []

        
    # Getter 
    def get_first_name(self):
        return f"My first name is {self._first_name}."
    
    def get_last_name(self):
        return f"My last name is {self._last_name}."
        
    def get_email(self):
        return f"My email is {self._email}."
    
    def get_department(self):
        return f"My department is {self._department}."
    
    def get_members(self):
        return self._members
       
    # Setter 
    def set_first_name(self, first_name):
        self._first_name = first_name
    
    def set_last_name(self, last_name):
        self._last_name = last_name
    
    def set_email(self, email):
        self._email = email
    
    def set_department(self, department):
        self._department = department 
    
    def set_members(self, object):
        self._first_name = object._first_name
        self._last_name = object._last_name

    # Abstract methods
    def get_full_name(self):
        full_name = f"{self._first_name} {self._last_name}"
        return full_name
        
    def add_request(self):
        pass
    
    def check_request(self):
        return f"Checking request"
    
    def add_user(self):
        pass
    
    def login(self):
        message = f"{self._email} has logged in"
        return message
    
    def logout(self):
        return f"{self._email} has logged out"
        
    def add_member(self, object):
        array =self._members.append(object)
        return array


            
# tl1 = TeamLead("Tony","Stark","tonyStark@gmail.com","Avengers")
# tl1.add_member("Thor","Odinson","godOfThunder@gmail.com","Marvel")   

class Admin(Person):
    def __init__(self, first_name, last_name, email, department):
        self._first_name = first_name
        self._last_name = last_name
        self._email = email
        self._department = department
        
    # Getter 
    def get_first_name(self):
        return f"My first name is {self._first_name}."
    
    def get_last_name(self):
        return f"My last name is {self._last_name}."
        
    def get_email(self):
        return f"My email is {self._email}."
    
    def get_department(self):
        return f"My department is {self._department}."
       
    # Setter 
    def set_first_name(self,first_name):
        self._first_name = first_name
    
    def set_last_name(self,last_name):
        self._last_name = last_name
    
    def set_email(self,email):
        self._email = email
    
    def set_department(self,department):
        self._department = department
    
    # Abstract methods
    def get_full_name(self):
        full_name = f"{self._first_name} {self._last_name}"
        return full_name
        
    def add_request(self):
       pass
    
    def check_request(self):
        pass
    
    def add_user(self):
        return "User has been added"   
    
    def login(self):
        message = f"{self._email} has logged in"
        return message
    
    def logout(self):
        return f"{self._email} has logged out"
        
class Request():
    def __init__(self, name, requester, dateRequested,):
        super().__init__()
        self._name = name
        self._requester = f"{requester._first_name} {requester._last_name}"
        self._dateRequested = dateRequested
        self._status = "Pending"
        
    def update_request(self):
        return f"{self._name} has been updated."
    
    def close_request(self):
        return f"{self._name} has been closed."
        
    def cancel_request(self):
        return f"{self._name} has been cancelled."
        
     # Getter 
    def get_name(self):
        return f"My first name is {self._name}."
    
    def get_requester(self):
        return f"My last name is {self._requester}."
        
    def get_dateRequested(self):
        return f"My email is {self._dateRequested}."
    
    def get_status(self):
        return f"My status is {self._status}."
    
     # Setter
    def get_name(self, name):
        self._name = name
        
    def get_requester(self, requester):
        self._requester = requester
        
    def get_dateRequested(self, date_requested):
        self._dateRequested = date_requested
    
    def set_status(self, status):
        self._status = status
        





# Test cases:
employee1 = Employee("John", "Doe", "djohn@mail.com", "Marketing")
employee2 = Employee("Jane", "Smith", "sjane@mail.com", "Marketing")
employee3 = Employee("Robert", "Patterson", "probert@mail.com", "Sales")
employee4 = Employee("Brandon", "Smith", "sbrandon@mail.com", "Sales")
admin1 = Admin("Monika", "Justin", "jmonika@mail.com", "Marketing")
teamLead1 = TeamLead("Michael", "Specter", "smichael@mail.com", "Sales")
req1 = Request("New hire orientation", teamLead1, "27-Jul-2021")
req2 = Request("Laptop repair", employee1, "1-Jul-2021")

assert employee1.get_full_name() == "John Doe", "Full name should be John Doe"
assert admin1.get_full_name() == "Monika Justin", "Full name should be Monika Justin"
assert teamLead1.get_full_name() == "Michael Specter", "Full name should be Michael Specter"
assert employee2.login() == "sjane@mail.com has logged in"
assert employee2.add_request() == "Request has been added"
assert employee2.logout() == "sjane@mail.com has logged out"

teamLead1.add_member(employee3)
teamLead1.add_member(employee4)

for indiv_emp in teamLead1.get_members():
    print(indiv_emp.get_full_name())

assert admin1.add_user() == "User has been added"

req2.set_status("closed")
print(req2.close_request())





        

    
    
        